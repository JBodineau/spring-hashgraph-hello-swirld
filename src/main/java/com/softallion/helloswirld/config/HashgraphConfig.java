package com.softallion.helloswirld.config;

import com.softallion.helloswirld.hashgraph.HelloSwirldDemoMain;
import com.softallion.spring.hashgraph.config.EnableHashgraph;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableHashgraph(HelloSwirldDemoMain.class)
public class HashgraphConfig {

}
