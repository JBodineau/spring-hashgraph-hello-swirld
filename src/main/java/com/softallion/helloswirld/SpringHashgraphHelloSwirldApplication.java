package com.softallion.helloswirld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

@SpringBootApplication
@ComponentScan(basePackages = {"com.softallion.helloswirld"})
public class SpringHashgraphHelloSwirldApplication {

    private static final Logger logger = LoggerFactory.getLogger(SpringHashgraphHelloSwirldApplication.class);

    public static void main(String[] args) {
        Environment env = SpringApplication.run(SpringHashgraphHelloSwirldApplication.class, args).getEnvironment();

        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            logger.warn("The host name could not be determined, using `localhost` as fallback");
        }
        logger.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\t{}://localhost:{}\n\t" +
                        "External: \t{}://{}:{}\n\t" +
                        "Swagger: \t\t{}://localhost:{}/swagger-ui\n\t" +
                        "Profile(s): \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                env.getProperty("server.port"),
                protocol,
                hostAddress,
                env.getProperty("server.port"),
                protocol,
                env.getProperty("server.port"),
                env.getActiveProfiles());
    }
}
