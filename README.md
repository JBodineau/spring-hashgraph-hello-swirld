# spring-hashgraph-hello-swirld

Adaptation of the hello-swirld example project from swirld to the spring ecosystem (by using the spring-hashgraph library).

---

**This project is just a project to show how we could easily start a new Spring-boot project based on the hashgraph algorithm if we adapt the swirld SDK.**

**So, since the spring-hashgraph referenced below is not existing on any repository, you cannot actually download it.** 

---

# Why use the spring-hashgraph library?

You're familiar with the spring ecosystem? 
You just want to start one (and only one) application based on the hashgraph consensus algorithm and you dont care about the swirld browser operations?

So, you can use this library in a spring boot application. When you will start your application, the swirld browser will be automatically started.


# How to start


## Swirld hashgraph

Before starting, it's important to understand the hashgraph concepts. So, you can start by [downloading the swirld SDK](https://www.swirlds.com/download/) and read the documentation inside.


## Initialize the spring boot application

Use one of your existing application (the one where you want to add the hashgraph capabilities), or [start a new spring boot application](https://start.spring.io/).


## Add the spring-hashgraph dependency

With maven, you can import the spring-hashgraph library :

```xml
<dependency>
      <groupId>com.softallion</groupId>
      <artifactId>spring-hashgraph</artifactId>
      <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Enable hashgraph

In your application configuration, you have to enable hashgraph by addind @EnableHashgraph annotation in a @Configuration annotated class.
The EnableHashgraph annotation has one argument : the class of your SwirldMain implementation.

```java
@Configuration
@EnableHashgraph(HelloSwirldDemoMain.class)
public class HashgraphConfig {
    //...
}
```

## Configuration

Configuration is almost the same than the swirld config (config.txt).See the [application.yml](https://gitlab.com/JBodineau/spring-hashgraph-hello-swirld/blob/master/src/main/resources/config/application.yml) file.



## Run

Since this is a spring boot application, by default, when you start the application, the AWT headless mode is enabled.
For the moment, the front-end is an AWT based front-end.

To make it runnable, the easiest way is to specify the next JVM option :
```
-Djava.awt.headless=false
```



And that's it! You can now implements your hashgraph based application by using the entire spring ecosystem!


# Next ?

- Include the swirld SDK needed datas to the library (derby database, databse recovery, key tools, ...)
- Integrate the others config.txt properties
- Migrate the front-app (from awt to webapp : angular or viewjs?) in order to improve the exploitation/control tools
- A lot of interesting things to do. Imagination is just the limit :)

